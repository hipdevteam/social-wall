import { createContext } from 'react';

const AllFeedsContext = createContext(null);

export default AllFeedsContext;
