import ReactDOM from 'react-dom';

import SocialWall from './SocialWall';

ReactDOM.render(SocialWall, document.getElementById('sbsw-app'));
