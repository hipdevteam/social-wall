<?php

namespace SB\SocialWall;

/**
 * Class Deactivate.
 *
 * Handles plugin deactivation.
 */
class Deactivate {

	/**
	 * Deactivation hook.
	 *
	 * @return void
	 */
	public static function handle() {}
}
